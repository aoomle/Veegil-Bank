//
//  ViewController.swift
//  Veegil Bank
//
//  Created by Abdulmalik Muhammad on 06/05/2021.
//

import UIKit

class MainTabbar: UITabBarController {

  override func viewDidLoad() {
    super.viewDidLoad()
    self.tabBar.tintColor = .label
    viewControllers = [
      generateController(view: Home(), title: "", tabBarIcon: "house"),
      generateController(view: TransferController(), title: "", tabBarIcon: "paperplane"),
      generateController(view: StatictsController(), title: "", tabBarIcon: "chart.bar"),
      generateController(view: SettingsController(), title: "", tabBarIcon: "gear")
    ]
  }
  
  fileprivate func generateController(view controller: UIViewController, title: String, tabBarIcon: UITabBarItem.SystemItem) -> UINavigationController{
    let navController = UINavigationController(rootViewController: controller)
    navController.title = title
    navController.tabBarItem = UITabBarItem(tabBarSystemItem: tabBarIcon, tag: 0)
    navController.tabBarItem.title = title
    return navController
  }
  
  fileprivate func generateController(view controller: UIViewController, title: String, tabBarIcon: String) -> UINavigationController{
    let navController = UINavigationController(rootViewController: controller)
    navController.title = title
    navController.tabBarItem = UITabBarItem(title: title, image: UIImage(systemName: tabBarIcon), tag: 0)
    navController.tabBarItem.title = title
    return navController
  }
}


class Home: UIViewController {
  
  override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = .systemBackground
    navigationController?.setNavigationBarHidden(true, animated: false)
  }
  
}


class TransferController: UIViewController {
  
  override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = .systemBlue
    navigationController?.setNavigationBarHidden(true, animated: false)
  }
  
}


class StatictsController: UIViewController {
  
  override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = .systemGreen
    navigationController?.setNavigationBarHidden(true, animated: false)
  }
  
}

class SettingsController: UIViewController {
  
  override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = .systemOrange
    navigationController?.setNavigationBarHidden(true, animated: false)
  }
  
}
